<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',['as' => 'home', function () {
    return view('home.index');
}]);


Route::get('/escenarios', function () {
    return view('escenarios.escenarios');
})->name('escenarios');

Route::get('/musica', function () {
    return view('musica.musica');
})->name('musica');

Route::get('/contacto', function () {
    return view('contacto.contacto');
})->name('contacto');

Route::get('/anuncios', function () {
    return view('anuncios.anuncios');
})->name('anuncios');

Route::get('/login', function () {
    return view('login.passwords');
})->name('login');

Route::post('/contacto', 'ControllerContacto@store')->name('contacto');


//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
