@extends('home.template')

@section('contenido')

<section id="cta" class="wrapper style3" style="background: linear-gradient(to right,#141E30, #243B55);">
					<div class="container">
						<header>
						<h2><a href="index.html" id="logo">CONTACTO</a></h2>
						
						</header>
					</div>
				</section>
<section class="wrapper style1" style="background: linear-gradient(to right,#649173, #DBD5A4);">
<div class="container">


				





<form action="{{route('contacto')}}" method="POST">
@csrf
<h1>Nombre</h1>
{!!$errors->first('nombre','<small style="color:red">:message</small><br>')!!} 
<input type="text" name="nombre" value="{{old('nombre')}}"><br>

<h1>Correo Electronico</h1>
{!!$errors->first('email','<small style="color:red">:message</small><br>')!!} 
<input type="text" name="email" value="{{old('email')}}"><br>
<h1>Asunto</h1>
{!!$errors->first('asunto','<small style="color:red">:message</small><br>')!!} 
<input type="text" name="asunto" value="{{old('asunto')}}"><br>
<h1>Descripcion</h1>
{!!$errors->first('contenido','<small style="color:red">:message</small><br>')!!} 
<textarea name="contenido">{{old('contenido')}}</textarea><br>



<header class="major">
	<button class="button" style =  " background-color:#fdbb2d;">{{__('Enviar')}}</button>
</header>

</form>

</div>
</section>
@endsection