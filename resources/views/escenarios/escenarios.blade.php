@extends('home.template')

@section('contenido')
<!-- Main -->
<section id="cta" class="wrapper style3" style="background: linear-gradient(to right,#141E30, #243B55);">
					<div class="container">
						<header>
						<h2><a href="index.html" id="logo">Proyectos de vivienda en Venta</a></h2>
						
						</header>
					</div>
				</section>
				<section class="wrapper style1" style="background: linear-gradient(to right,#649173, #DBD5A4);" >
					<div class="container">
						<div class="row">
						
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa1.jpg" alt=""  /></a>
									<div class="inner">
										<h3>Centro</h3>
										<h4>Desde:  $244,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas</p>
									</div>
								</div>
							</section>
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa2.jpg" alt="" /></a>
									<div class="inner">
										<h3>Las Palmas</h3>
										<h4>Desde:  $444,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas</p>
									</div>
								</div>
							</section>
						</div>
						<div class="row">
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa7.jpg" alt="" /></a>
									<div class="inner">
										<h3>Tepeyac</h3>
										<h4>Desde: $378,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas</p>
									</div>
								</div>
							</section>
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa4.jpg" alt="" /></a>
									<div class="inner">
										<h3>El Plan</h3>
										<h4>Desde:  $900,000,00</h4>
										<p> AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas</p>
									</div>
								</div>
							</section>
							
						</div>
						<div class="row">
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa6.jpg" alt="" /></a>
									<div class="inner">
										<h3>Santa Elena</h3>
										<h4>Desde:  $501,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas</p>
									</div>
								</div>
							</section>
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa1.jpg" alt="" /></a>
									<div class="inner">
										<h3>El Refugio</h3>
										<h4>Desde:  $874,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas</p>
									</div>
								</div>
							</section>
							
						</div>
						<div class="row">
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa8.jpg" alt="" /></a>
									<div class="inner">
										<h3>Vista Hermosa</h3>
										<h4>Desde:  $644,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas.</p>
									</div>
								</div>
							</section>
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa9.jpg" alt="" /></a>
									<div class="inner">
										<h3>Cañada</h3>
										<h4>Desde:  $944,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas.</p>
									</div>
								</div>
							</section>
							
						</div>
						<div class="row">
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa10.jpg" alt="" /></a>
									<div class="inner">
										<h3>Buena Vista</h3>
										<h4>Desde: $344,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas.</p>
									</div>
								</div>
							</section>
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="https://www.google.com.mx/maps/preview" class="image left"><img src="images/casa11.jpg" alt="" /></a>
									<div class="inner">
										<h3>La luz</h3>
										<h4>Desde: $293,000,00</h4>
										<p>AMENIDADES INTERIORES, Alcoba ppal con baño y vestier, 2 Alcobas auxiliares, Baño auxiliar, Baño social, Cocina, Comedor, Estudio, Patio, Sala, Zona de ropas.</p>
									</div>
								</div>
							</section>
							
						</div>
					
					
					
				</section>
				<section id="cta" class="wrapper style3" style="background: linear-gradient(to right,#141E30, #243B55);">
					<div class="container">
						<header>
							
							<h2><a href="index.html" id="logo">¿Tu casa en Mexico esta a un paso?</a></h2>
							<a href="https://viventa.co/?s=&cities-new=&sector-proyecto=&rango-precios=&post_type=projects-new" class="button" style =  " background-color:#fdbb2d;">Cotizar</a>
						</header>
					</div>
				</section>



@endsection

