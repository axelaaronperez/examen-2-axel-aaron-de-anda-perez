<!DOCTYPE HTML>
<!--
	Arcana by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Proyecto Viventa</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="is-preload" >
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Logo -->
						<h1><a href="index.html" id="logo">Viventa Inmobiliaria<em>2021</em></a></h1>
						
					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li class="current"><a href="{{route('home')}}">Quienes Somos?</a></li>
								<li><a href="{{route('escenarios')}}">Venta inmuebles</a></li>
								<li><a href="{{route('musica')}}">Renta inmuebles</a></li>
								<li><a href="{{route('anuncios')}}">Anuncios</a></li>
								<li><a href="{{route('contacto')}}">Contacto</a></li>
								<li><a href="{{route('login')}}">Login</a></li>
								
								
							</ul>
						</nav>

				</div>

			<!-- Banner -->
				<section id="banner">
					<header>
						<h2>El placer de invertir <em>en Viventa Mexico desde el exterior<a href=""></a></em></h2>
						<a href="https://mexico.electricdaisycarnival.com/tickets/" class="button" style =  " background-color:#fdbb2d;">Quiero una asesoria!</a>
					</header>
				</section>

                @yield('contenido')
                <!-- Footer -->
				<div id="footer" style="background: linear-gradient(to right, #e9d362, #333333);">
					<div class="container">
						<div class="row">
							<section class="col-3 col-6-narrower col-12-mobilep">
								
								<ul class="links">
									<li><a href="https://mexico.electricdaisycarnival.com/contact-us/">Contacto</a></li>
									<li><a href="https://www.insomniac.com/web-accessibility-statement/">Accesibilidad Web</a></li>
									<li><a href="https://www.ocesa.com.mx/aviso-de-privacidad">Politicas de Privacidad</a></li>
									<li><a href="https://privacyportal.onetrust.com/webform/ba6f9c5b-dda5-43bd-bac4-4e06afccd928/55dec3f9-962e-49de-ae71-ea2296e76ffa">No vendas mi Informacion</a></li>
									<li><a href="https://www.insomniac.com/covid-19-waiver/">COVID-19 Waiver</a></li>
									
								</ul>
							</section>
							<section class="col-3 col-6-narrower col-12-mobilep">
								
								<ul class="links">
									<li><a href="https://mexico.electricdaisycarnival.com/contact-us/">Contacto</a></li>
									<li><a href="https://www.insomniac.com/web-accessibility-statement/">Accesibilidad Web</a></li>
									<li><a href="https://www.ocesa.com.mx/aviso-de-privacidad">Politicas de Privacidad</a></li>
									<li><a href="https://privacyportal.onetrust.com/webform/ba6f9c5b-dda5-43bd-bac4-4e06afccd928/55dec3f9-962e-49de-ae71-ea2296e76ffa">No vendas mi Informacion</a></li>
									<li><a href="https://www.insomniac.com/covid-19-waiver/">COVID-19 Waiver</a></li>
								</ul>
							</section>
							<section class="col-6 col-12-narrower">
								<h3>Contactanos</h3>
								<form>
									<div class="row gtr-50">
										<div class="col-6 col-12-mobilep">
											<input type="text" name="name" id="name" placeholder="Name" />
										</div>
										<div class="col-6 col-12-mobilep">
											<input type="email" name="email" id="email" placeholder="Email" />
										</div>
										<div class="col-12">
											<textarea name="message" id="message" placeholder="Message" rows="5"></textarea>
										</div>
										<div class="col-12">
											<ul class="actions">
												<li><input type="submit" class="button alt" value="Enviar Mensaje" /></li>
											</ul>
										</div>
									</div>
								</form>
							</section>
						</div>
					</div>

					<!-- Icons -->
					<div class = "center">
						<ul class="icons">
							<li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon brands fa-github"><span class="label">GitHub</span></a></li>
							<li><a href="#" class="icon brands fa-linkedin-in"><span class="label">LinkedIn</span></a></li>
							<li><a href="#" class="icon brands fa-google-plus-g"><span class="label">Google+</span></a></li>
						</ul>

					</div>
						
						

					<!-- Copyright -->
						<div class="copyright">
							<ul class="menu">
								<li>&copy;Todos los derechos reservados.<a href="https://mexico.electricdaisycarnival.com/">Al continuar después de esta página, estás de acuerdo con nuestra política de uso. ©2020 Insomniac Holdings, LLC.</a></li>
							</ul>
						</div>

				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>