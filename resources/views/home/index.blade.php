@extends('home.template')

@section('contenido')

<!-- Highlights -->
			
<section class="wrapper style1" style="background: linear-gradient(to right, #3E5151, #DECBA4);">>>
	
					<div class="container">
					
						<div class="row gtr-200">
							<div class="col-lg-8 col-sm-12">
								
								<div class="embed-responsive embed-responsive-16by9">
								<iframe width="853" height="480" src="https://www.youtube.com/embed/Wof8RczBfkU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
							
							<section class="col-4 col-12-narrower">

								
								<div class="box highlight">
									<span class="image featured"><img src="images/gastos.jpg" alt="" /></span>
									<h3>Los gastos adicionales al comprar vivienda en Mexico</h3>
									<p>Cuando estás en el proceso de comprar una vivienda en Mexico y escuchas hablar de los “gastos adicionales”, probablemente empieces a...</p>
								</div>
							</section>
							<section class="col-4 col-12-narrower">
								<div class="box highlight">
									<span class="image featured"><img src="images/buscarcasa.jpg" alt="" /></span>
									<h3>5 Mitos al comprar vivienda en Mexico desde el exterior</h3>
									<p>Mito: (sustantivo) Del griego mŷthos. Persona o cosa a la que se atribuyen cualidades o excelencias que no tiene. En otras palabras: un...</p>
								</div>
							</section>
							<section class="col-4 col-12-narrower">
								<div class="box highlight">
									<span class="image featured"><img src="images/viventain.jpg" alt="" /></span>
									<h3 >Materializa tus esfuerzos: compra tu vivienda en Mexico (¡Viventa Inmobiliaria!)</h3>
									<p>Si nos preguntamos cuándo empezamos a soñar con tener una casa propia, tenemos que hacer un viaje al pasado: probablemente desde que...</p>
								</div>
							</section>
							<section class="col-4 col-12-narrower">
								<div class="box highlight">
									<span class="image featured"><img src="images/bbb.jpg" alt="" /></span>
									<h3 >Crédito hipotecario: ¿Te conviene pagarlo anticipadamente? ¡Conoce los pros y los contras!</h3>
									<p>Si eres uno de los muchos propietarios de vivienda que pagan cumplidamente sus cuotas hipotecarias mensuales, es posible que hayas considerado la opción de pagar tu deuda</p>
								</div>
							</section>
							
						</div>
					</div>
				</section>

			<!-- Gigantic Heading -->
				<section class="wrapper style2" style="background: linear-gradient(to right, #e9d362, #DECBA4);">
					<div class="container">
						<header class="major">
							<span class="image featured"><img src="" alt="" /></span>
							<h2><a href="index.html" id="logo">QUIENES SOMOS?</a></h2>
							<p>Te ayudamos a invertir en vivienda en Mexico con seguridad. Por más de 15 años hemos ayudado a miles de Mexicanos en el exterior a hacer realidad su sueño de invertir en vivienda en Mexico: sin tener que viajar, con confianza, seguridad y facilidad. Tenemos presencia en San Juan, La Chona, Lagos de Moreno, Guadalajara, y CDMX; sin embargo, ¡nuestra oficina es el mundo y te asesoramos dónde estés!</p>
						</header>
					</div>
				</section>

			<!-- Posts -->
			<section id="cta" class="wrapper style3" style="background: linear-gradient(to right,#141E30, #243B55);">
					<div class="container">
						<header>
						<h2><a href="index.html" id="logo">NUESTRO EQUIPO DE TRABAJO</a></h2>
						
						</header>
					</div>
				</section>
				<section class="wrapper style1">
					<div class="container">
						<div class="row">
						
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="#" class="image left"><img src="images/mf.jpg" alt="" /></a>
									<div class="inner">
										<h3>Goreti</h3>
										<p>Es un ingeniero líder e innovador, capaz de producir soluciones y mejoras en los procesos de las organizaciones creando Sistemas Computacionales, Soluciones de Software y Tecnologías de Información.</p>
									</div>
								</div>
							</section>
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="" class="image left"><img src="images/hm.jpg" alt="" /></a>
									<div class="inner">
										<h3>Juan Antonio</h3>
										<p>Es un ingeniero líder e innovador, capaz de producir soluciones y mejoras en los procesos de las organizaciones creando Sistemas Computacionales, Soluciones de Software y Tecnologías de Información.</p>
									</div>
								</div>
							</section>
						</div>
						<div class="row">
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="#" class="image left"><img src="images/hm.jpg" alt="" /></a>
									<div class="inner">
										<h3>Luis Daniel</h3>
										<p>Es un ingeniero líder e innovador, capaz de producir soluciones y mejoras en los procesos de las organizaciones creando Sistemas Computacionales, Soluciones de Software y Tecnologías de Información.</p>
									</div>
								</div>
							</section>
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="#" class="image left"><img src="images/hm.jpg" alt="" /></a>
									<div class="inner">
										<h3>Axel Perez</h3>
										<p> Es un ingeniero líder e innovador, capaz de producir soluciones y mejoras en los procesos de las organizaciones creando Sistemas Computacionales, Soluciones de Software y Tecnologías de Información.</p>
									</div>
								</div>
							</section>
							
						</div>
						<div class="row">
							<section class="col-6 col-12-narrower">
								<div class="box post">
									<a href="#" class="image left"><img src="images/hm.jpg" alt="" /></a>
									<div class="inner">
										<h3>Juan Carlos</h3>
										<p>Es un ingeniero líder e innovador, capaz de producir soluciones y mejoras en los procesos de las organizaciones creando Sistemas Computacionales, Soluciones de Software y Tecnologías de Información.</p>
									</div>
								</div>
							</section>
							
						</div>
						
					</div>
				</section>

				
	

			<!-- CTA -->
				<section id="cta" class="wrapper style3" style="background: linear-gradient(to right,#141E30, #243B55);">
					<div class="container">
						<header>
						<h2><a href="index.html" id="logo">¿Quieres ser parte de nuestro equipo?</a></h2>
							
							<a href="https://mexico.electricdaisycarnival.com/guide/hours-info/" class="button" style =  " background-color:#fdbb2d;">Solicitar</a>
						</header>
						
					</div>
				</section>
				
                @endsection