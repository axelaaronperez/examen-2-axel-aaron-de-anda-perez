<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageReceived;
use App\Mail\MessageAnswer;
class ControllerContacto extends Controller
{
    public function store(){


        $mens = request()->validate([
            'nombre'=>'required',
            'email'=>'required|email',
            'asunto'=>'required',
            'contenido'=>'required|min:5'

        ]);

        Mail::to('axelaaron1996@gmail.com')->send(new MessageReceived($mens));

        Mail::to(request('email'))->send(new MessageAnswer);
      return "Mensaje Enviado";
        
    }
}
